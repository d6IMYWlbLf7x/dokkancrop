/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef guard_61215c3f5969f84bc1854b36b12a0e1e
#define guard_61215c3f5969f84bc1854b36b12a0e1e

#include <iosfwd>
#include <memory>

class DokkanCrop {
	public:
		class ProgressListener {
			public:
				virtual void setMaxProgressValue(size_t) = 0;
				virtual void resetProgressValue() = 0;
				virtual void incrementProgressValue() = 0;
				virtual ~ProgressListener() noexcept {}
		};

		enum class MergeMode {
			VERTICAL,
			HORIZONTAL,
			BOTH
		};

		explicit DokkanCrop(char const* path);

		void addInputFile(std::string const& fileName);
		void clearInputFiles();
		void mergeImages(std::string const& outputFile) const;
		void setMergeMode(MergeMode);
		MergeMode getMergeMode() const;
		void setProgressListener(ProgressListener&);

		DokkanCrop(DokkanCrop&&) = default;
		DokkanCrop& operator=(DokkanCrop&&) = default;

		~DokkanCrop() noexcept;

	private:
		DokkanCrop(DokkanCrop const&) = delete;
		DokkanCrop& operator=(DokkanCrop const&) = delete;

		struct PImpl;
		std::unique_ptr<DokkanCrop::PImpl>
			d;
};

#endif //defined guard_61215c3f5969f84bc1854b36b12a0e1e
