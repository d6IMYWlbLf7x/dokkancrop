/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef guard_363a86c5129b12721025efd2037a3846
#define guard_363a86c5129b12721025efd2037a3846

#include "DokkanCropQtMainWindow.hxx"

class DokkanCropQt {
	public:
		DokkanCropQt(int& argc, char** const argv) :
			m_app(argc, argv),
			m_window(argv[0], m_app)
		{}

		int exec() {
			return m_app.exec();
		}

	private:
		DokkanCropQt(DokkanCropQt&&) = delete;
		DokkanCropQt(DokkanCropQt const&) = delete;
		DokkanCropQt& operator=(DokkanCropQt&&) = delete;
		DokkanCropQt& operator=(DokkanCropQt const&) = delete;

		QApplication
			m_app;
		DokkanCropQtMainWindow
			m_window;
};

#endif //defined guard_363a86c5129b12721025efd2037a3846
