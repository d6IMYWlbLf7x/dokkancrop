/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef guard_839160b45225c20e64a7ff8ae80a4de0
#define guard_839160b45225c20e64a7ff8ae80a4de0

#include <QtWidgets>

class LicenseWindow : public QWidget {
	public:
		LicenseWindow() {
			this->resize(360, 360);
			this->setWindowTitle("DokkanCrop License");

			QTextEdit*
				licenseArea = new QTextEdit(this);
			licenseArea->setReadOnly(true);
			licenseArea->setText(licenseText);
			licenseArea->setWordWrapMode(QTextOption::WrapMode::NoWrap);

			QVBoxLayout*
				licenseLayout = new QVBoxLayout(this);
			licenseLayout->setSpacing(2);
			licenseLayout->setContentsMargins(2, 2, 2, 2);
			licenseLayout->addWidget(licenseArea);
		}

		virtual ~LicenseWindow() noexcept override {}

	private:
		static char const*
			licenseText;
};

#endif //defined guard_839160b45225c20e64a7ff8ae80a4de0
