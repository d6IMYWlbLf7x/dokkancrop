/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef guard_d3efce27395f988053d4214ff2b0e2e4
#define guard_d3efce27395f988053d4214ff2b0e2e4

#include <QtWidgets>
#include <QtGlobal>

class AboutWindow : public QWidget {
	public:
		AboutWindow() {
			this->setWindowTitle("About DokkanCrop");

			QLabel*
				aboutLabel = new QLabel(this);
			aboutLabel->setText(
				"<b>DokkanCrop</b><br>"
				"Qt5 frontend<br>"
				"Qt version: " QT_VERSION_STR "<br>"
				"<br>"
				"Licensed under GPLv3 or later.<br>"
				"See \"About->License\" for the full version of the license.<br>"
				"<br>"
				"Source code repository:<br>"
				"<a href=\"https://github.com/d6IMYWlbLf7x/dokkancrop\">https://github.com/d6IMYWlbLf7x/dokkancrop</a>"
			);
			aboutLabel->setTextFormat(Qt::RichText);
			aboutLabel->setTextInteractionFlags(Qt::TextBrowserInteraction);
			aboutLabel->setOpenExternalLinks(true);
			aboutLabel->setAlignment(Qt::AlignmentFlag::AlignHCenter | Qt::AlignmentFlag::AlignVCenter);

			QVBoxLayout*
				aboutLayout = new QVBoxLayout(this);
			aboutLayout->setSpacing(20);
			aboutLayout->setContentsMargins(20, 20, 20, 20);
			aboutLayout->addWidget(aboutLabel);
		}

		virtual ~AboutWindow() noexcept override {}
};

#endif //defined guard_d3efce27395f988053d4214ff2b0e2e4
