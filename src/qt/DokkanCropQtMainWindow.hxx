/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef guard_6e8ba54fc08e938824ce9324d06af4ea
#define guard_6e8ba54fc08e938824ce9324d06af4ea

#include "AboutWindow.hxx"
#include "LicenseWindow.hxx"

#include <QtWidgets>
#include <dokkancrop.hxx>
#include <limits>

#ifdef __GNUG__
#include <cxxabi.h>
#endif

class DokkanCropQtMainWindow : public QMainWindow, public DokkanCrop::ProgressListener {
	public slots:

		void exit() {
			r_app.get().exit();
		}

		void setHorizontalMergeMode() {
			m_dc.setMergeMode(DokkanCrop::MergeMode::HORIZONTAL);
			if (m_vAct) {
				m_vAct->setChecked(false);
			}
			if (m_hAct) {
				m_hAct->setChecked(true);
			}
			if (m_bAct) {
				m_bAct->setChecked(false);
			}
		}

		void setVerticalMergeMode() {
			m_dc.setMergeMode(DokkanCrop::MergeMode::VERTICAL);
			if (m_vAct) {
				m_vAct->setChecked(true);
			}
			if (m_hAct) {
				m_hAct->setChecked(false);
			}
			if (m_bAct) {
				m_bAct->setChecked(false);
			}
		}

		void setBothMergeMode() {
			m_dc.setMergeMode(DokkanCrop::MergeMode::BOTH);
			if (m_vAct) {
				m_vAct->setChecked(false);
			}
			if (m_hAct) {
				m_hAct->setChecked(false);
			}
			if (m_bAct) {
				m_bAct->setChecked(true);
			}
		}

		void openFileDialog() {
			auto const
				fileNameList = QFileDialog::getOpenFileNames(this, "Add images", "", "Images (*.png *.jpg *.jpeg);;All files (*)");
			if (fileNameList.size() > 0) {
				m_statusBar->showMessage(QString().sprintf("Added %d file(s).", fileNameList.size()));
			}
			m_fileList->addItems(fileNameList);
		}

		void saveFileDialog() {
			if (m_fileList->count() > 0) {
				auto const
					fileName = QFileDialog::getSaveFileName(this, "Save merged image as...", "", "Images (*.png *.jpg *.jpeg);;All files (*)");
				if (fileName != "") {
					m_dc.clearInputFiles();
					for (int i = 0; i < m_fileList->count(); ++i) {
						m_dc.addInputFile(m_fileList->item(i)->text().toStdString());
					}
					m_statusBar->showMessage("Processing...");
					try {
						m_dc.mergeImages(fileName.toStdString());
						m_statusBar->showMessage("Merged image saved as: " + fileName);
					}
					catch (std::exception& e) {
						m_statusBar->showMessage((std::string("Error: ") + e.what()).c_str());
						m_progressBar->setValue(0);

						char const*
							typeName = typeid(e).name();

						#ifdef __GNUG__
							std::size_t
								len = 0;
							int
								status = 0 ;
							std::unique_ptr<char, decltype(&std::free)>
								namePtr(__cxxabiv1::__cxa_demangle(typeName, nullptr, &len, &status), &std::free);
							typeName = namePtr.get();
						#endif

						QMessageBox::critical(this, typeName, e.what());
					}
				}
			}
			else {
				QMessageBox::warning(this, "No input files", "No images selected for merging.");
			}
		}

		void clearFileList() {
			m_fileList->clear();
		}

		void removeSelected() {
			delete m_fileList->takeItem(m_fileList->currentRow());
		}

		void moveUp() {
			auto const
				row = m_fileList->currentRow();
			m_fileList->insertItem(row - 1, m_fileList->takeItem(row));
			m_fileList->setCurrentRow(row - 1);
		}

		void moveDown() {
			auto const
				row = m_fileList->currentRow();
			m_fileList->insertItem(row + 1, m_fileList->takeItem(row));
			m_fileList->setCurrentRow(row + 1);
		}

		void openAboutWindow() {
			m_aboutWindow.show();
			m_aboutWindow.activateWindow();
		}

		void openLicenseWindow() {
			m_licenseWindow.show();
			m_licenseWindow.activateWindow();
		}

	public:

		DokkanCropQtMainWindow(char const* const path, QApplication& app) :
			m_dc(path),
			r_app(std::ref(app)),
			m_window(new QWidget(this)),
			m_aboutWindow(),
			m_licenseWindow(),
			m_fileList(new QListWidget(m_window)),
			m_progressBar(new QProgressBar(m_window)),
			m_statusBar(new QStatusBar(m_window)),
			m_hAct(nullptr),
			m_vAct(nullptr),
			m_bAct(nullptr)
		{
			this->setCentralWidget(m_window);
			this->resize(300, 300);
			this->setWindowTitle("DokkanCrop (Qt5 frontend)");

			m_fileList->show();

			QMenu*
				fileMenu = new QMenu(m_window);
			fileMenu->setTitle("&File");

			auto
				addAct = fileMenu->addAction("&Add images...");
			addAct->setIcon(this->style()->standardIcon(QStyle::SP_DialogOpenButton, nullptr, this));
			addAct->setShortcut(QKeySequence::StandardKey::Open);
			connect(addAct, &QAction::triggered, this, &DokkanCropQtMainWindow::openFileDialog);

			auto
				saveAct = fileMenu->addAction("Merge and &save as...");
			saveAct->setIcon(this->style()->standardIcon(QStyle::SP_DialogSaveButton, nullptr, this));
			saveAct->setShortcut(QKeySequence::StandardKey::Save);
			connect(saveAct, &QAction::triggered, this, &DokkanCropQtMainWindow::saveFileDialog);

			fileMenu->addSeparator();

			auto
				moveUpAct = fileMenu->addAction("Move selected &up");
			moveUpAct->setIcon(this->style()->standardIcon(QStyle::SP_ArrowUp, nullptr, this));
			connect(moveUpAct, &QAction::triggered, this, &DokkanCropQtMainWindow::moveUp);

			auto
				moveDownAct = fileMenu->addAction("Move selected &down");
			moveDownAct->setIcon(this->style()->standardIcon(QStyle::SP_ArrowDown, nullptr, this));
			connect(moveDownAct, &QAction::triggered, this, &DokkanCropQtMainWindow::moveDown);

			auto
				removeSelectedAct = fileMenu->addAction("&Remove selected");
			removeSelectedAct->setIcon(this->style()->standardIcon(QStyle::SP_DialogDiscardButton, nullptr, this));
			connect(removeSelectedAct, &QAction::triggered, this, &DokkanCropQtMainWindow::removeSelected);

			auto
				clearAct = fileMenu->addAction("&Clear image list");
			clearAct->setIcon(this->style()->standardIcon(QStyle::SP_TrashIcon, nullptr, this));
			connect(clearAct, &QAction::triggered, this, &DokkanCropQtMainWindow::clearFileList);

			fileMenu->addSeparator();

			auto
				exitAct = fileMenu->addAction("&Exit");
			exitAct->setIcon(this->style()->standardIcon(QStyle::SP_DialogCloseButton, nullptr, this));
			exitAct->setShortcut(QKeySequence::StandardKey::Quit);
			connect(exitAct, &QAction::triggered, this, &DokkanCropQtMainWindow::exit);

			fileMenu->show();

			QMenu*
				mergeModeMenu = new QMenu(m_window);
			mergeModeMenu->setTitle("&Merge mode");
			m_hAct = mergeModeMenu->addAction("&Horizontal");
			m_hAct->setCheckable(true);
			m_hAct->setChecked(false);
			connect(m_hAct, &QAction::triggered, this, &DokkanCropQtMainWindow::setHorizontalMergeMode);
			m_vAct = mergeModeMenu->addAction("&Vertical");
			m_vAct->setCheckable(true);
			m_vAct->setChecked(false);
			connect(m_vAct, &QAction::triggered, this, &DokkanCropQtMainWindow::setVerticalMergeMode);
			m_bAct = mergeModeMenu->addAction("&Both");
			m_bAct->setCheckable(true);
			m_bAct->setChecked(true);
			m_dc.setMergeMode(DokkanCrop::MergeMode::BOTH);
			connect(m_bAct, &QAction::triggered, this, &DokkanCropQtMainWindow::setBothMergeMode);
			mergeModeMenu->show();

			QMenu*
				optionsMenu = new QMenu(m_window);
			optionsMenu->setTitle("&Options");
			optionsMenu->addMenu(mergeModeMenu);
			optionsMenu->show();

			QMenu*
				helpMenu = new QMenu(m_window);
			helpMenu->setTitle("&Help");
			auto
				licenseAct = helpMenu->addAction("&License...");
			connect(licenseAct, &QAction::triggered, this, &DokkanCropQtMainWindow::openLicenseWindow);
			auto
				aboutAct = helpMenu->addAction("&About");
			aboutAct->setIcon(this->style()->standardIcon(QStyle::SP_DialogHelpButton, nullptr, this));
			connect(aboutAct, &QAction::triggered, this, &DokkanCropQtMainWindow::openAboutWindow);
			helpMenu->show();

			QMenuBar*
				menu = new QMenuBar(m_window);
			menu->setNativeMenuBar(true);
			menu->addMenu(fileMenu);
			menu->addMenu(optionsMenu);
			menu->addMenu(helpMenu);
			menu->show();

			QToolButton*
				plusButton = new QToolButton(m_window);
			plusButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogOpenButton, nullptr, this));
			plusButton->setToolTip("Add images");
			plusButton->show();
			connect(plusButton, &QToolButton::released, this, &DokkanCropQtMainWindow::openFileDialog);

			QToolButton*
				minusButton = new QToolButton(m_window);
			minusButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogDiscardButton, nullptr, this));
			minusButton->setToolTip("Remove selected image from the list");
			minusButton->show();
			connect(minusButton, &QToolButton::released, this, &DokkanCropQtMainWindow::removeSelected);

			QToolButton*
				clearButton = new QToolButton(m_window);
			clearButton->setIcon(this->style()->standardIcon(QStyle::SP_TrashIcon, nullptr, this));
			clearButton->setToolTip("Clear image list");
			clearButton->show();
			connect(clearButton, &QToolButton::released, this, &DokkanCropQtMainWindow::clearFileList);

			QToolButton*
				saveButton = new QToolButton(m_window);
			saveButton->setIcon(this->style()->standardIcon(QStyle::SP_DialogSaveButton, nullptr, this));
			saveButton->setToolTip("Merge images and save as");
			saveButton->show();
			connect(saveButton, &QToolButton::released, this, &DokkanCropQtMainWindow::saveFileDialog);

			QToolButton*
				moveUpButton = new QToolButton(m_window);
			moveUpButton->setIcon(this->style()->standardIcon(QStyle::SP_ArrowUp, nullptr, this));
			moveUpButton->setToolTip("Move selected image up");
			moveUpButton->show();
			connect(moveUpButton, &QToolButton::released, this, &DokkanCropQtMainWindow::moveUp);

			QToolButton*
				moveDownButton = new QToolButton(m_window);
			moveDownButton->setIcon(this->style()->standardIcon(QStyle::SP_ArrowDown, nullptr, this));
			moveDownButton->setToolTip("Move selected image down");
			moveDownButton->show();
			connect(moveDownButton, &QToolButton::released, this, &DokkanCropQtMainWindow::moveDown);

			QToolBar*
				toolbar = new QToolBar(m_window);
			toolbar->addWidget(plusButton);
			toolbar->addWidget(saveButton);
			toolbar->addSeparator();
			toolbar->addWidget(moveUpButton);
			toolbar->addWidget(moveDownButton);
			toolbar->addWidget(minusButton);
			toolbar->addWidget(clearButton);
			toolbar->show();

			m_statusBar->show();
			m_progressBar->show();

			QVBoxLayout*
				layout = new QVBoxLayout(m_window);
			layout->setSpacing(2);
			layout->setContentsMargins(2, 2, 2, 0);
			layout->addWidget(menu);
			layout->addWidget(toolbar);
			layout->addWidget(m_fileList);
			layout->addWidget(m_progressBar);
			layout->addWidget(m_statusBar);

			m_dc.setProgressListener(*this);

			this->show();
		}

		virtual ~DokkanCropQtMainWindow() noexcept override {}

		/* Inherited from QMainWindow */

		virtual void closeEvent(QCloseEvent* event) override {
			m_aboutWindow.close();
			m_licenseWindow.close();
			QMainWindow::closeEvent(event);
		}

		/* Inherited from DokkanCrop::ProgressListener */

		virtual void setMaxProgressValue(size_t const newMax) override {
			size_t const
				intMax = static_cast<size_t>(std::numeric_limits<int>::max());
			if (newMax > intMax) {
				m_progressBar->setMaximum(std::numeric_limits<int>::max());
			}
			else {
				m_progressBar->setMaximum(static_cast<int>(newMax));
			}
		}

		virtual void resetProgressValue() override {
			m_progressBar->setValue(0);
			r_app.get().processEvents();
		}

		virtual void incrementProgressValue() override {
			m_progressBar->setValue(m_progressBar->value()+1);
			r_app.get().processEvents();
		}

	private:
		DokkanCropQtMainWindow(DokkanCropQtMainWindow&&) = delete;
		DokkanCropQtMainWindow(DokkanCropQtMainWindow const&) = delete;
		DokkanCropQtMainWindow& operator=(DokkanCropQtMainWindow&&) = delete;
		DokkanCropQtMainWindow& operator=(DokkanCropQtMainWindow const&) = delete;

		DokkanCrop
			m_dc;
		std::reference_wrapper<QApplication>
			r_app;
		QWidget
			*m_window;
		AboutWindow
			m_aboutWindow;
		LicenseWindow
			m_licenseWindow;
		QListWidget
			*m_fileList;
		QProgressBar
			*m_progressBar;
		QStatusBar
			*m_statusBar;
		QAction
			*m_hAct,
			*m_vAct,
			*m_bAct;
};

#endif //defined guard_6e8ba54fc08e938824ce9324d06af4ea
