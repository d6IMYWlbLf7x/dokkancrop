/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <dokkancrop.hxx>

#include <string>
#include <cmath>
#include <vector>
#include <numeric>
#include <list>
#include <experimental/optional>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wredundant-decls"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <Magick++.h>
#pragma GCC diagnostic pop

struct DokkanCrop::PImpl {
	PImpl() :
		mMode(MergeMode::BOTH),
		inputFiles(),
		progressListener()
	{}

	MergeMode
		mMode;
	std::list<std::string>
		inputFiles;
	std::experimental::optional<std::reference_wrapper<ProgressListener>>
		progressListener;

	private:
		PImpl& operator=(PImpl&&) = delete;
		PImpl& operator=(PImpl const&) = delete;
		PImpl(PImpl&&) = delete;
		PImpl(PImpl const&) = delete;
};

static bool
	s_isMagickInitialized = false;

DokkanCrop::~DokkanCrop() noexcept {}

void DokkanCrop::clearInputFiles() {
	d->inputFiles.clear();
}

void DokkanCrop::addInputFile(std::string const& fileName) {
	d->inputFiles.emplace_back(fileName);
}

void DokkanCrop::setMergeMode(DokkanCrop::MergeMode const mMode) {
	d->mMode = mMode;
}

DokkanCrop::MergeMode DokkanCrop::getMergeMode() const {
	return d->mMode;
}

void DokkanCrop::setProgressListener(DokkanCrop::ProgressListener& listener) {
	d->progressListener = listener;
}

inline void paste(Magick::Image const& from, Magick::Image& to, ssize_t const x, ssize_t const y) {
	to.modifyImage();
	auto
		fromPixels = from.getConstPixels(0, 0, from.columns(), from.rows());
	auto
		toPixels = to.setPixels(x, y, from.columns(), from.rows());
	for (size_t row = 0; row < from.rows(); ++row) {
		for (size_t column = 0; column < from.columns(); ++column) {
			*toPixels++=*fromPixels++;
		}
	}
	to.syncPixels();
}

void DokkanCrop::mergeImages(std::string const& outputFile) const {
	if (d->progressListener) {
		d->progressListener->get().setMaxProgressValue(d->inputFiles.size()*2+3);
		d->progressListener->get().resetProgressValue();
	}
	std::list<Magick::Image>
		images;
	//collect and crop all input images
	for (auto const& inputName : d->inputFiles) {
		images.emplace_back(inputName);
		//rotate the image if landscape orientation is detected
		if (images.back().columns() > images.back().rows()) {
			images.back().rotate(90.0);
		}
		Magick::Geometry
			croppedGeometry;
		auto const
			newWidth = 10.0*images.back().rows()/17.6;
		if (newWidth < images.back().columns()) {
			croppedGeometry.width(static_cast<size_t>(newWidth));
			croppedGeometry.xOff(static_cast<ssize_t>((images.back().columns()-newWidth)/2.0));
		}
		croppedGeometry.height(static_cast<size_t>(images.back().rows()*0.629));
		croppedGeometry.yOff(static_cast<ssize_t>(images.back().rows()*0.167));
		images.back().crop(croppedGeometry);
		if (d->progressListener) {
			d->progressListener->get().incrementProgressValue();
		}
	}

	//calculate the output image dimensions
	size_t
		outWidth = 1,
		outHeight = 1,
		numRowsCols = static_cast<size_t>(std::ceil(std::sqrt(images.size()))), //for "both" mode only
		rowWidth = 1; //for "both" mode only
	std::vector<size_t>
		rowHeights(numRowsCols, 1);
	{
		size_t
			i = 0; //for "both" mode only
		for (auto const& image : images) {
			switch (d->mMode) {
				case MergeMode::HORIZONTAL:
					if (outHeight < image.rows())
						outHeight = image.rows();
					outWidth += image.columns();
					break;
				case MergeMode::VERTICAL:
					if (outWidth < image.columns())
						outWidth = image.columns();
					outHeight += image.rows();
					break;
				case MergeMode::BOTH:
					if (i % numRowsCols == 0) {
						rowWidth = image.columns();
					}
					else {
						rowWidth += image.columns();
					}
					if (outWidth < rowWidth)
						outWidth = rowWidth;
					if (rowHeights[i/numRowsCols] < image.rows())
						rowHeights[i/numRowsCols] = image.rows();
					++i;
					break;
			}
		}
	}
	if (d->mMode == MergeMode::BOTH) {
		outHeight = std::accumulate(rowHeights.begin(), rowHeights.end(), size_t(1));
	}
	if (d->progressListener) {
		d->progressListener->get().incrementProgressValue();
	}

	if (!images.empty()) {
		Magick::Image
			out(Magick::Geometry(outWidth, outHeight), Magick::Color("Black"));
		if (d->progressListener) {
			d->progressListener->get().incrementProgressValue();
		}
		ssize_t
			x = 0,
			y = 0;
		size_t
			i = 0; //for "both" mode only
		for (auto iter = images.cbegin(); iter != images.cend();) {
			paste(*iter, out, x, y);
			switch (d->mMode) {
				case MergeMode::HORIZONTAL:
					x += static_cast<ssize_t>(iter->columns());
					break;
				case MergeMode::VERTICAL:
					y += static_cast<ssize_t>(iter->rows());
					break;
				case MergeMode::BOTH:
					++i;
					if (i % numRowsCols == 0) {
						x = 0;
						y += static_cast<ssize_t>(rowHeights[(i-1)/numRowsCols]);
					}
					else {
						x += static_cast<ssize_t>(iter->columns());
					}
					break;
			}
			iter = images.erase(iter);
			if (d->progressListener) {
				d->progressListener->get().incrementProgressValue();
			}
		}
		out.write(outputFile);
	}
	else {
		if (d->progressListener) {
			d->progressListener->get().incrementProgressValue();
		}
	}
	if (d->progressListener) {
		d->progressListener->get().incrementProgressValue();
	}
}

DokkanCrop::DokkanCrop(char const* path) :
	d(std::make_unique<DokkanCrop::PImpl>())
{
	if (!s_isMagickInitialized) {
		Magick::InitializeMagick(path);
		s_isMagickInitialized = true;
	}
}
