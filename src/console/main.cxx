/*
	Copyright (c) 2017  d6IMYWlbLf7x <wDUegt3uzgPo0s@tutanota.com>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <dokkancrop.hxx>

#include <string>
#include <iostream>
#include <experimental/optional>

int main(int const argc, char const* const* const argv) {
	DokkanCrop
		dc(argv[0]);
	std::experimental::optional<std::string>
		outputFileName;
	bool
		outputFileNameIsNow = false;
	for (int argno = 1; argno < argc; ++argno) {
		if (outputFileNameIsNow) {
			outputFileNameIsNow = false;
			outputFileName.emplace(argv[argno]);
		}
		else {
			std::string
				argstr(argv[argno]);
			if (argstr.compare("-h") == 0 || argstr.compare("--help") == 0 || argstr.compare("-help") == 0) {
				std::cerr << "Usage: dokkancrop [OPTIONS...] [INPUT_FILES...]" << std::endl;
				std::cerr << "Option list:" << std::endl;
				std::cerr << "\t -o FILENAME   -   specify the output file name" << std::endl;
				std::cerr << "\t -H            -   set the horizontal merge mode" << std::endl;
				std::cerr << "\t -V            -   set the vertical merge mode" << std::endl;
				std::cerr << "\t -B            -   set the vertical+horizontal merge mode" << std::endl;
				std::cerr << "\t -h            -   display this help" << std::endl;
				return EXIT_SUCCESS;
			}
			else if (argstr.compare("-o") == 0) {
				outputFileNameIsNow = true;
			}
			else if (argstr.compare("-H") == 0) {
				dc.setMergeMode(DokkanCrop::MergeMode::HORIZONTAL);
			}
			else if (argstr.compare("-V") == 0) {
				dc.setMergeMode(DokkanCrop::MergeMode::VERTICAL);
			}
			else if (argstr.compare("-B") == 0) {
				dc.setMergeMode(DokkanCrop::MergeMode::BOTH);
			}
			else {
				dc.addInputFile(argstr);
			}
		}
	}
	if (!outputFileName) {
		std::cerr << "Error: No output file name specified. Use -o to set it." << std::endl;
		return EXIT_FAILURE;
	}

	dc.mergeImages(*outputFileName);

	return EXIT_SUCCESS;
}
